#include<iostream>

#define MAXN 101

int n;
unsigned long long  g[MAXN][MAXN], gp[MAXN][MAXN], tmp[MAXN][MAXN], MOD = 1000*1000*1000+7;

void bingpow(int l){
    for(int i = 0; i < n; ++i)
        gp[i][i] = 1;
    while(l){
        if(l & 1){
            for(int i = 0; i < n; ++i)
                for(int j = i; j < n; ++j)
                    for(int k = 0; k < n; ++k)
                        tmp[i][j] += (gp[i][k] * g[k][j]) % MOD;
            for(int i = 0; i < n; ++i)
                for(int j = i; j < n; ++j){
                    gp[i][j] = gp[j][i] = tmp[i][j] % MOD;
                    tmp[i][j] ^= tmp[i][j];
                }
        }
        for(int i = 0; i < n; ++i)
            for(int j = i; j < n; ++j)
                for(int k = 0; k < n; ++k)
                    tmp[i][j] += (g[i][k] * g[k][j]) % MOD;
        for(int i = 0; i < n; ++i)
            for(int j = i; j < n; ++j){
                g[i][j] = g[j][i] = tmp[i][j] % MOD;
                tmp[i][j] ^= tmp[i][j];
            }
        l >>= 1;
    }
}

int main(){
    int m, u, v, l, p ,q;
    std::cin >> n >> m;
    std::cin >> u >> v >> l;
    for(int i = 0; i < m; ++i){
        std::cin >> p >> q;
        ++g[p-1][q-1];
        ++g[q-1][p-1];
    }
    bingpow(l);
    std::cout << gp[u-1][v-1];
    return 0;
}
