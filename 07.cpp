#include<bits/stdc++.h>

using namespace std;


int n, m, to;

bool usd[10000+2];
vector<int> g[10000+2];
queue<int> q;

void bfs(int s){
    usd[s] = true;
    q.push(s);
    while(!q.empty()){
        to = q.front();
        q.pop();
        for(auto v: g[to])
            if(!usd[v]){
                usd[v] = true;
                q.push(v);
            }
    }
}

int main(){
    int u, v, re = 0;
    cin >> n >> m;
    for(int i = 0; i < m ; ++i){
        cin >> u >> v;
        g[v-1].push_back(u-1);
        g[u-1].push_back(v-1);
    }
    for(int i = 0; i < n; ++i)
        if(!usd[i]){
            ++re;
            bfs(i);
        }
    cout << re - 1;
    return 0;
}
