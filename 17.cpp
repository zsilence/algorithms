#include<iostream>


int main()
{
    int n, m, x1, y1, x2, y2;
    std::cin >> n >> m >> x1 >> y1 >> x2 >> y2;
    if(abs(x1 - x2) == abs(y1 - y2))
        std::cout << "NO";
    else
        std::cout << "YES";
    return 0;
}
