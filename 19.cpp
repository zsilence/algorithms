#include<iostream>


int main()
{
    long n;
    long a;
    bool q[7] = {0};
    std::cin >> n;
    for(long i = 0; i < n; ++i)
    {
        std::cin >> a;
        if(a == 1021*1031*1033)
            q[6] = true;
        else if(a == 1031*1033)
            q[5] = true;
        else if(a == 1021*1033)
            q[4] = true;
        else if(a == 1021*1031)
            q[3] = true;
        else if(a == 1033)
            q[2] = true;
        else if(a == 1031)
            q[1] = true;
        else if(a == 1021)
            q[0] = true;
    }
    if(q[6] || (q[0] && q[1] && q[2]) || (q[5] && q[0]) || (q[4] && q[1]) || (q[3] && q[2]))
        std::cout << "YES";
    else
        std::cout << "NO";

    return 0;
}
