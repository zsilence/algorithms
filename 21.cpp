#include<bits/stdc++.h>

using namespace std;

#define pr pair<int, int>
#define dx run[it][0]
#define dy run[it][1]

int n, m, i, j;

bool usd[101][101];
int d[101][101];
queue<pr> q;
pr to;
int run[8][2] = {{-2, -1}, {-1, -2}, {2, -1}, {1, -2}, {-2, 1}, {-1, 2}, {1, 2}, {2, 1}};

void bfs(int u, int v){
    int x, y;
    usd[u][v] = true;
    d[u][v] = 0;
    q.push(pr(u, v));
    while(!q.empty()){
        to = q.front();
        x = to.first;
        y = to.second;
        q.pop();
        for(int it = 0; it < 8; ++it)
            if(n > x + dx && x + dx >= 0 && m > y + dy && y + dy >= 0 && !usd[x+dx][y+dy]){
                usd[x+dx][y+dy] = true;
                d[x+dx][y+dy] = d[x][y] + 1;
                if(x + dx == i && y + dy == j) return;
                q.push(pr(x+dx, y+dy));
            }
    }
}

int main(){
    cin >> n >> m >> i >> j;
    --i; --j;
    bfs(0, 0);
    !usd[i][j] ? cout << "NEVAR" : cout << d[i][j];
    return 0;
}
