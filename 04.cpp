#include<iostream>


int n;
int a[1000*1000 + 1];
int s[1000*1000 + 1];
long long cnt;

void merge(int l, int m, int r){
    int i = 0, j = 0;
    while((i < m - l) && (j < r - m))
        if(a[l + i] <= a[m + j]){
            s[i + j] = a[l + i];
            i++;
        } else {
            s[i + j] = a[m + j];
            cnt += m - (l + i);
            j++; 
        }
    while(i < m - l){
        s[i + j] = a[l + i];
        i++;
    }
    while(j < r - m){
        s[i + j] = a[m + j];
        j++;
    }
    for(int k = 0; k < i + j; ++k)
        a[l + k] = s[k];
}

void merge_sort(int l, int r){
    if(l >= r - 1)
        return;    
    int m = (r + l) / 2;
    merge_sort(l, m);
    merge_sort(m, r);
    merge(l, m, r);
}

int main(){
    std::cin >> n;
    for(int i = 0; i < n; ++i)
        std::cin >> a[i];
    merge_sort(0, n);
    std::cout << cnt;
    return 0;
}
