#include<bits/stdc++.h>

using namespace std;

typedef long long ll;


int main(){
    int n, k;
    int a[100000+1];
    int m = 1000*1000*1000+7;
    ll r = 1;
    int y = 0, j = 0;
    cin >> n >> k;
    for(int i = 0; i < n; ++i)
        cin >> a[i];
    sort(a, a + n);
    if((a[n-1] <= 0 && k % 2) || k == n)
        for(int i = n - 1; i > n - 1 - k; --i){
            r = (1LL * r * a[i]) % m;
            if(r < 0) r += m;
        }
    else{
        if(k % 2){
            r = (1LL * r * a[n-1]) % m;
            --k; --n;
        }
        while(y + j < k)
            if(1LL * a[y] * a[y+1] > 1LL * a[n-1-j] * a[n-2-j]){
                r = m + (1LL * r * a[y++]) % m;
                r = m + (1LL * r * a[y++]) % m;
            } else {
                r = (1LL * r * a[n-1-j++]) % m;
                r = (1LL * r * a[n-1-j++]) % m;
            }
    }
    cout << r;
    return 0;
}
