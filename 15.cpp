#include<bits/stdc++.h>

using namespace std;


int main()
{
    string s;
    int q, l, r;
    int a[100003] = {0}, b[100003] = {0};
    cin >> s;
    cin >> q;
    for(int i = 0; i < q; ++i)
    {
        cin >> l >> r;
        if(l > r)
            swap(l, r);    
        a[l-1]++;
        b[r-1]++; 
    }
    int c = 0;
    for(int i = 0; i < s.length(); ++i)
    {
        c += a[i];
        if(c % 2) 
            if(isupper(s[i]))
                cout << (char)tolower(s[i]);
            else
                cout << (char)toupper(s[i]);
        else
            cout << s[i];
        c -= b[i];
    }
    return 0;
}
