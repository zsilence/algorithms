#include<bits/stdc++.h>

using namespace std;

#define pr pair<int, int>

typedef long long ll;

int n, m, to;
ll inf = 1000LL*1000*1000*1000*1000;

ll d[100000+2];
vector<pr> g[100000+2];
set<pr> q;

void dkstr(int s, int e){
    d[s] = 0;
    q.insert(pr(d[s], s));
    while(!q.empty()){
        to = q.begin()->second;
        if(to == e) return;
        q.erase(q.begin());
        for(auto v: g[to])
            if(d[v.first] > d[to] + v.second){
                if(d[v.first] == inf) q.erase(pr(d[v.first], v.first));
                d[v.first] = d[to] + v.second;
                q.insert(pr(d[v.first], v.first));
            }
    }
}

int main(){
    int u, v, c;
    cin >> n;
    cin >> m;
    for(int i = 0; i < n; ++i)
        d[i] = inf;
    for(int i = 0; i < m ; ++i){
        cin >> u >> v >> c;
        g[v-1].push_back(pr(u-1, c));
        g[u-1].push_back(pr(v-1, c));
    }
    cin >> u;
    cin >> v;
    dkstr(--u, --v);
    cout << d[v];
    return 0;
}
