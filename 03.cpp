#include<bits/stdc++.h>

using namespace std;


int main(){
    int res = -1;
    string s;
    cin >> s;
    int n = s.length();
    for(int i = 0; 2 * i < n; ++i)
        if(s[i] != s[n - i - 1]){
            res = n;
            break;
        }
    if(res != n)
        for(int i = 1; i < n; ++i)
            if(s[i] != s[i - 1]){
                res = n - 1;
                break;
            }
    cout << res;
    return 0;
}
